﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGenerator : MonoBehaviour {

	
	#region Attributs 

	public int _width, _height;  // Largeur et hauteur du labyrinthe 
	public VisualCell visualCellPrefab; // Prefab qui sert de modèle a l'instanciation 
	public Cell[,] cells; // Tableau de cellule à deux dimensions
	private Vector2 _randomCellPos; // Position de la cellule aléatoire qui va commencer la generation
	private VisualCell visualCellInst; // Contient la copie du prefab de l'instanciation 
	private List<CellAndRelativePosition> neighbors; // Liste des cellules voisines 
	#endregion

	#region Méthodes

	void Start()
	{
		cells = new Cell[_width, _height]; // Initialisation du tableau de cellules 
		Init(); 
	}

	void Init()
	{
		for (int i = 0; i < _width; i++)
		{
			for (int j = 0; j < _height; j++)
			{
				cells[i, j] = new Cell(false, false, false, false, false);
				cells[i, j].xPos = i;
				cells[i, j].zPos = j;
			}
		}

		RandomCell(); // Lance la fonction RandomCell 
		InitVisualCell(); // Lance l'instanciation des cellules visuelles 
	}

	void RandomCell()
	{
		// Recupere une position X et Y aléatoire
		_randomCellPos = new Vector2((int) UnityEngine.Random.Range(0, _width), (int) UnityEngine.Random.Range(0, _height));
		
		//Lance la fonction GenearateMaze avec la position X et Y aléatoire 
		GenerateMaze((int) _randomCellPos.x, (int) _randomCellPos.y);
	}

	void GenerateMaze(int x, int y)
	{
		Cell currentCell = cells[x, y]; // Definit la cellule courante 
		neighbors = new List<CellAndRelativePosition>(); // Initialise la liste 
		if (currentCell._visited == true) return;
		currentCell._visited = true;

		if (x + 1 < _width && cells[x + 1, y]._visited == false)
		{
			
			neighbors.Add(new CellAndRelativePosition(cells[x + 1, y], CellAndRelativePosition.Direction.East));
		}

		if (y + 1 < _height && cells[x, y + 1]._visited == false)
		{
			neighbors.Add(new CellAndRelativePosition(cells[x, y + 1], CellAndRelativePosition.Direction.South));
		}

		if (x - 1 >= 0 && cells[x - 1, y]._visited == false)
		{
			neighbors.Add(new CellAndRelativePosition(cells[x - 1, y], CellAndRelativePosition.Direction.West));
		}

		if (y - 1 >= 0 && cells[x, y - 1]._visited == false)
		{
			neighbors.Add(new CellAndRelativePosition(cells[x, y - 1], CellAndRelativePosition.Direction.North));
		}

		if (neighbors.Count == 0) return;

		neighbors.Shuffle();

		foreach (CellAndRelativePosition selectedcell in neighbors)
		{
			if (selectedcell.direction == CellAndRelativePosition.Direction.East)
			{
				if(selectedcell.cell._visited) continue;
				currentCell._Est = true;
				selectedcell.cell._West = true;
				GenerateMaze(x + 1, y);
			}
			
			else if (selectedcell.direction == CellAndRelativePosition.Direction.South)
			{
				if(selectedcell.cell._visited) continue;
				currentCell._South = true;
				selectedcell.cell._North = true;
				GenerateMaze(x, y + 1); 
			}
			
			else if (selectedcell.direction == CellAndRelativePosition.Direction.West)
			{
				if(selectedcell.cell._visited) continue;
				currentCell._West = true;
				selectedcell.cell._Est = true;
				GenerateMaze(x - 1, y); 
			}
			
			else if (selectedcell.direction == CellAndRelativePosition.Direction.North)
			{
				if(selectedcell.cell._visited) continue;
				currentCell._North = true;
				selectedcell.cell._South = true;
				GenerateMaze(x, y - 1);
			}
		}
	}

	void InitVisualCell()
	{
		foreach (Cell cell in cells)
		{
			visualCellInst = Instantiate(visualCellPrefab, new Vector3(cell.xPos * 3, 0, _height * 3f - cell.zPos * 3),	Quaternion.identity) as VisualCell;
			visualCellInst.transform.parent = transform;
			visualCellInst._North.gameObject.SetActive(!cell._North);
			visualCellInst._South.gameObject.SetActive(!cell._South);
			visualCellInst._Est.gameObject.SetActive(!cell._Est);
			visualCellInst._West.gameObject.SetActive(!cell._West);

			visualCellInst.transform.name = cell.xPos.ToString() + "_" + cell.zPos.ToString();
		}
	}


	#endregion
}
