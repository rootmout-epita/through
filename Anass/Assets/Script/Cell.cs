﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell
{
    public bool _West, _North, _Est, _South; //Mur Ouest, Nord, Est et Sud 
    public bool _visited; //Booleen pour savoir si oui ou non la cellule a été visitée 
    
    public int xPos, zPos; 

    public Cell(bool west, bool north, bool est, bool south, bool visited) // Constructeur 
    {
        this._West = west;
        this._North = north;
        this._Est = est;
        this._South = south;
        this._visited = visited; 
        
    }
}
