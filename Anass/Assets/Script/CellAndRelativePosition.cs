﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellAndRelativePosition
{
    public Cell cell; // Cellule 
    public Direction direction; // Direction à prendre lors de la suppression d'un mur

    public enum Direction
    {
        North,
        South, 
        East, 
        West
    }

    public CellAndRelativePosition(Cell cell, Direction direction)
    {
        this.cell = cell;
        this.direction = direction;
    }
}
