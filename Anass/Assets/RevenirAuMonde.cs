﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RevenirAuMonde : MonoBehaviour {

	#region Attributs

	public Button back;
	#endregion

	void Start () 
	{
		back.onClick.AddListener (OnClickBack);
	}
	

	void Update () 
	{

		Time.timeScale = 0f;
	}

	void OnClickBack ()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#endif 

		//Application.LoadLevel ("");
	}
		
}
