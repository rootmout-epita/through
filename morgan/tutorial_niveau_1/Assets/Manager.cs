﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Constraints;
using UnityEngine;

public class Manager : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		PhotonNetwork.ConnectUsingSettings("1.0");
		PhotonNetwork.offlineMode = false;
		Debug.Log("Ceci est un test");
	}
	
	// Update is called once per frame
	//void Update () {
	//	
	//}

	void OnJoinedLobby()
	{
		Debug.Log("Lobby Joined");
		PhotonNetwork.JoinRandomRoom();
	}

	void OnJoinedRoom()
	{
		Debug.Log("Room Joined, have Fun ,o/");
		GameObject player = PhotonNetwork.Instantiate("Player", Vector3.zero + Vector3.up * 4 + Vector3.left *3+ Vector3.back*3, Quaternion.identity, 0);
		//player.GetComponent<Input>().enabled = true;
		
	} 

	void OnPhotonRandomJoinFailed(){
	
		Debug.Log("Failed to join a room...");
		PhotonNetwork.CreateRoom("room");
	}
}
