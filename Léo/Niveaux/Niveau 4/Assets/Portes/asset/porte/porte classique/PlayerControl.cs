﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {
	public float movespeed;
	public float runspeed;
	private CharacterController Player;
	public float Jump = 2;
	public float Sensibility = 5;
	public int gravity = 12;
	private Vector3 DirectionDeplacement = Vector3.zero;

	// Use this for initialization
	void Start () {
		Player = GetComponent<CharacterController> ();

		
		
	}
	
	// Update is called once per frame
	void Update () {

			DirectionDeplacement.x = Input.GetAxisRaw ("Horizontal");
			DirectionDeplacement.z = Input.GetAxisRaw ("Vertical");
		DirectionDeplacement = transform.TransformDirection (DirectionDeplacement);

		if (Input.GetKey (KeyCode.LeftShift)) {
			Player.Move (DirectionDeplacement * Time.deltaTime * runspeed);
		} else {
			Player.Move (DirectionDeplacement * Time.deltaTime * movespeed);

		}
		if (Input.GetKeyDown (KeyCode.Space) && Player.isGrounded) {
			DirectionDeplacement.y = Jump;
		}
		if (!Player.isGrounded) {
			DirectionDeplacement.y = DirectionDeplacement.y - gravity * Time.deltaTime;
		}

		transform.Rotate (0f, Input.GetAxisRaw ("Mouse X") * Sensibility, 0f);
		//transform.Rotate (Input.GetAxisRaw ("Mouse Y"), 0f * Sensibility, 0f);
		//DirectionDeplacement.y = 0f;


	}
}
