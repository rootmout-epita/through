using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
//using UnityEditor;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Manager : Photon.MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		PhotonNetwork.ConnectUsingSettings("1.0");
		PhotonNetwork.offlineMode = false;
		Debug.Log("Ceci est un test");
	}
	
	// Update is called once per frame
	void Update () {
		if (!IsGameBegin && PhotonNetwork.playerList.Length > 1 && IAmTheRoomCreator)
		{
			Debug.Log("STARTGAME");
			player.transform.position = new Vector3(-19,2,6);
            PhotonView photonView = PhotonView.Get(this);
            photonView.RPC("ChangeStateOfTheGame", PhotonTargets.All, null);
        }

		if (IsGameBegin && PhotonNetwork.playerList.Length < 2)
		{
            //Il n'y a plus assez de joueurs dans la parie :/
            StartCoroutine(Timeoutdeco());
		}
	}

	private bool IAmTheRoomCreator = false;
	private bool IsGameBegin = false;
	private GameObject player;

	void OnJoinedLobby()
	{
		Debug.Log("Lobby Joined");
		PhotonNetwork.JoinRandomRoom();
	}

	void OnJoinedRoom()
	{
		Debug.Log("Room Joined, have Fun ,o/");
		Vector3 spawn = IAmTheRoomCreator ? Vector3.up * 2 + Vector3.left * 2 + Vector3.forward * 3:Vector3.up * 2 + Vector3.right * 8 + Vector3.forward * 5;
		player = PhotonNetwork.Instantiate("MyPlayer", spawn, Quaternion.identity, 0);
		player.GetComponent<FirstPersonController>().enabled = true;//Activation du joueur.
		player.GetComponentInChildren<Camera>().enabled = true;//Activation de sa camera.
		player.GetComponentInChildren<AudioListener>().enabled = true;//Activation de son micro
		SetLayerRecursively(player,9);
	} 
	
	void SetLayerRecursively(GameObject obj, int newlayer)
	{
		obj.layer = newlayer;

		foreach (Transform child in obj.transform)
		{
			SetLayerRecursively(child.gameObject, newlayer);
		}
	}

	void OnPhotonRandomJoinFailed()
	{
		Debug.Log("Failed to join a room...");
		IAmTheRoomCreator = true;
		PhotonNetwork.CreateRoom("room");
	}

	public void EndOfTheGame()
	{
		PhotonView photonView = PhotonView.Get(this);
		photonView.RPC("EndOfTheGame_Network", PhotonTargets.AllViaServer, null);
	}
	
	[PunRPC]
	void ChangeStateOfTheGame()
	{
		IsGameBegin = !IsGameBegin;
		PhotonNetwork.room.open = !IsGameBegin;
	}

	[PunRPC]
	void EndOfTheGame_Network()
	{
        Debug.Log("Nique");
        PhotonNetwork.Disconnect();
        Application.LoadLevel("Scene_FinModeMulti");
    }

    IEnumerator Timeoutdeco()
    {
        yield return new WaitForSecondsRealtime(3);
        PhotonNetwork.Disconnect();
        Application.LoadLevel("Scene_Pertedunjoueur");
    }
}
