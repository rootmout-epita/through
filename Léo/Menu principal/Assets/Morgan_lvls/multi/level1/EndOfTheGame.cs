﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfTheGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider je_suis_un_collider)
	{
		//throw new System.NotImplementedException();
		GameObject TheManager = GameObject.Find("Manager");
		TheManager.GetComponent<Manager>().EndOfTheGame();
	}
}
