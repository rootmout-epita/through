﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class MenuPause : MonoBehaviour {

	#region Attributs 

	public Button resume;
	public Button quitter;
	public Button options;
	public GameObject canvas; 
	#endregion
	



	void Start()
	{
		resume.onClick.AddListener (OnClickResume);
		quitter.onClick.AddListener (OnClickQuitter);
		options.onClick.AddListener (OnClickOptions);
		Time.timeScale = 1.0f;
	}
	

	void Update()
	{
		if (Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Joystick1Button7))
		{
			canvas.SetActive(true);
			Time.timeScale = 0f;
		}
	}


	void OnClickResume()
	{
		canvas.SetActive (false);
		Time.timeScale = 1.0f;
	}

	void OnClickOptions()
	{

	}

	void OnClickQuitter()
	{
		Application.LoadLevel ("Scene_MenuPrincipal");
	}
}
