﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collide : MonoBehaviour {
	public Animator anim;

	private void Start()
	{
		anim.SetBool("open", false);
	}

	void OnTriggerExit (Collider je_suis_un_collider)
	{
			anim.SetBool ("open", !anim.GetBool("open"));
	}


}