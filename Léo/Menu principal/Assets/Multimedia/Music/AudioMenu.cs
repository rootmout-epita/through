﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMenu : MonoBehaviour {


	//public class MyUnitySingleton : MonoBehaviour {

	private static AudioMenu instance = null;
	public static AudioMenu Instance {
			get { return instance; }
		}
		void Awake() {
			if (instance != null && instance != this) {
				Destroy(this.gameObject);
				return;
			} else {
				instance = this;
			}
			DontDestroyOnLoad(this.gameObject);
		}
		// any other methods you need

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
