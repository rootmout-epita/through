﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnnoucementManager : MonoBehaviour {

	bool CycleEnCours = false;
	public AudioClip[] annonces;
	public AudioSource source;
	GameObject MenuMusicToDestroy;

	void Awake () {
		if (GameObject.Find ("MenuAudioManager") != null) {
			MenuMusicToDestroy = GameObject.Find ("MenuAudioManager");
			Destroy (MenuMusicToDestroy);
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!CycleEnCours && !source.isPlaying) {
			StartCoroutine (manager());
		}
	}


	IEnumerator manager() {
		CycleEnCours = true;
		transform.GetChild(0).gameObject.SetActive(true);
		int WaitTime = Random.Range (30, 90);
		//WaitTime = 10;
		yield return new WaitForSecondsRealtime (WaitTime);
		source.clip = annonces[Random.Range(0, annonces.Length)];
		source.Play();
		yield return new WaitForSecondsRealtime (1);
		transform.GetChild(0).gameObject.SetActive(false);
		CycleEnCours = false;
	}
}
