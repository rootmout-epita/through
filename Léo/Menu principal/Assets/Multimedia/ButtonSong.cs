﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Audio;

public class ButtonSong : MonoBehaviour {
	
	AudioSource source;
	public AudioClip clip;
	
	private void Start()
	{
		source = this.GetComponent<AudioSource>();
	}

	public void ExecuteMoi()
	{
		source.clip = clip;
		source.Play ();
	}
}
