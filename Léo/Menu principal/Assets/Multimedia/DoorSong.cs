﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class DoorSong : MonoBehaviour {

	Animator door;
	public AudioSource doorsource;
	public AudioClip dooreffect;
	private bool doorstate;

	// Use this for initialization
	void Start ()
	{
		door = this.transform.parent.gameObject.GetComponent<Animator>();
		doorstate = door.GetBool ("open");		
	}
	
	// Update is called once per frame
	void Update () {
		if (doorstate != door.GetBool("open"))
			{
			doorsource.clip = dooreffect;
			doorsource.Play ();
			doorstate = door.GetBool ("open");	
			}
	}
}
