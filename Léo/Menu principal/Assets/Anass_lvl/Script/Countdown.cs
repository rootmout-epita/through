﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour 
{
	[SerializeField] private Text uiText;
	[SerializeField] private float mainTimer;

	private float timer;
	private bool canCount = true; 
	private bool doOnce = false; 

	void Start()
	{
		timer = mainTimer;
	}

	void Update()
	{
		if (timer >= 0.0f && canCount) 
		{
			timer -= Time.deltaTime;
			uiText.text = timer.ToString ("F");
			PlayerPrefs.SetFloat("temps_final", 120f - timer);
		} 

		else if (timer <= 0.0f && !doOnce) 
		{
			canCount = false;
			doOnce = true; 
			uiText.text = "0.00";
			timer = 0.0f;
			GameOver ();
		}
	}



	void GameOver()
	{
		PlayerPrefs.DeleteKey("temps_final");
		Application.LoadLevel ("Scene_VraiGameOver");
	}




}
