﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {

	#region Attributs 
	public Button RETRY; 
	public Button QUIT;
	public Text temps_f;
	
	#endregion 

	void Start () 
	{
		
		RETRY.onClick.AddListener (OnClickRETRY);
		QUIT.onClick.AddListener (OnClickQUIT);
		temps_f.text = "Your time is : " + PlayerPrefs.GetFloat("temps_final").ToString();
		PlayerPrefs.DeleteKey("temps_final");
	}
	

	void Update () 
	{
		
	}

	void OnClickRETRY()
	{
		Application.LoadLevel ("Labyrinthe");
	}

	void OnClickQUIT()
	{
		Application.LoadLevel ("Scene_MenuPrincipal");
	}
}
