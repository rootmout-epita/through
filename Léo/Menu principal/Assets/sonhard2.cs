﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class sonhard2 : MonoBehaviour {

	public Slider slider;
	public AudioMixer audio;
	private float volume_son;

	
	void Start ()
	{
		volume_son = PlayerPrefs.GetFloat("volume_son", 0.5f);
		audio.SetFloat("MyExposedParam", volume_son);
		slider.value = volume_son;
	}
	
	
	void Update ()
	{
		audio.SetFloat("MyExposedParam", slider.value);
		PlayerPrefs.SetFloat("volume_son", slider.value);
		
		PlayerPrefs.Save();	
		
	}
}

