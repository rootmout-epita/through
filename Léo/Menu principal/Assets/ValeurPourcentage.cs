﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValeurPourcentage : MonoBehaviour {

     public Text pourcentageText;


    // Use this for initialization
    void Start()
    {
        pourcentageText = GetComponent<Text>();
    }

    // Update is called once per frame

    public void textUpdate(float value)
    {
        pourcentageText.text = Mathf.RoundToInt(value*100) + "%";
    }
}
