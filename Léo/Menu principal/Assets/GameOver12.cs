﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver12 : MonoBehaviour {

    #region Attributs 
    public Button QUIT;
   
	
    #endregion 

    void Start () 
    {
		
        QUIT.onClick.AddListener (OnClickQUIT);
    }
	

    void Update () 
    {
		
    }


    void OnClickQUIT()
    {
        Application.LoadLevel ("Scene_MenuPrincipal");
    }
}
