﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Pricolagepourdebut : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(StartingPlayer());
	}
	
	IEnumerator StartingPlayer()
	{
		yield return new WaitForSecondsRealtime(1);
		this.GetComponent<FirstPersonController>().enabled = true;
	}
	
}
