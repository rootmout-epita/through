﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour {
    
    public AudioMixer audiomixer;
    public void SetVolume(float volume)
    {       
        audiomixer.SetFloat("Main", volume);
    }
}
