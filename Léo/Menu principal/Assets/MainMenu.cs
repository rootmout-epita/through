﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public void QuitGame()
	{
		Application.Quit();
	}

	public void HardToMain()
	{
		//SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex - 6);
		Application.LoadLevel("Scene_MenuPrincipal");
	}
	
	public void MenuToMulti()
	{
		Application.LoadLevel("multi level1");
	}
	
	public void MenuToOptions()
	{
		Application.LoadLevel("Menu option groupe La confrérie");
	}

	public void TutoToMenu()
	{
		Application.LoadLevel("Scene_MenuPrincipal");
	}
	
	public void W1LVL1()
	{     		
		Application.LoadLevel("Niveau 1");
	}
	
	public void W1LVL2()
	{		
		Application.LoadLevel("Niveau 2");
	}
	
	public void W1LVL3()
	{		
		Application.LoadLevel("Niveau 3");
	}
	
	public void W1LVL4()
	{		
		Application.LoadLevel("Niveau 4");
	}
	
	
	
	
	public void W2LVL1()
	{		
		Application.LoadLevel("monde1_niveau1");
	}
	
	public void W2LVL2()
	{		
		Application.LoadLevel("niveau2");
	}
	
	public void W2LVL3()
	{		
		Application.LoadLevel("morgan");
	}
	
	public void W2LVL4()
	{		
		Application.LoadLevel("niveau4");
	}
	
	
	
	public void W3LVL1()
	{		
		Application.LoadLevel("Niveaux1");
	}
	
	public void W3LVL2()
	{		
		Application.LoadLevel("Niveaux2");
	}
	
	public void W3LVL3()
	{		
		Application.LoadLevel("Niveaux3");
	}
	
	public void W3LVL4()
	{		
		Application.LoadLevel("Niveaux4");
	}
	
	
	
	
	public void W4LVL1()
	{		
		Application.LoadLevel("Niveau_carre");
	}
	
	public void W4LVL2()
	{		
		Application.LoadLevel("Niveau_rond");
	}
	
	public void W4LVL3()
	{		
		Application.LoadLevel("Niveau_WW");
	}
	
	public void W4LVL4()
	{		
		Application.LoadLevel("Niveau_pacman");
	}
	


	
	public void MenutoTuto()
	{		
		Application.LoadLevel("Scene_Tutoriel");
	}
	
	public void TutoToW1()
	{		
		Application.LoadLevel("Scene_World1.5");
	}
	
	public void W1toW2()
	{		
		Application.LoadLevel("Scene_World2");
	}
	
	public void W2toW3()
	{		
		Application.LoadLevel("Scene_World3");
	}
	
	public void W3toW4()
	{		
		Application.LoadLevel("Scene_World4");
	}
	
	public void W4toHard()
	{		
		Application.LoadLevel("Scene_HardocreMode");
	}
	
	
	public void TUTOSTART()
	{		
		Application.LoadLevel("tutorial_level1");
	}

	public void HardStart()
	{
		Application.LoadLevel("Labyrinthe");
	}
	
}
