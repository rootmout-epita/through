﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.XR.WSA;

public class PathFinding2 : MonoBehaviour
{
	public Transform seeker, target;
	
	private Grid grid;

	void Start()
	{
		grid = GetComponent<Grid>();
	}

	private void Update()
	{
		try
		{
			grid.CreateGrid();
			List<Node> path = FindPath(seeker.position, target.position);
			if (path.Count > 0)
			{
				//seeker.Translate((path[0].worldPosition - seeker.position).normalized * Time.deltaTime * 4);
				var targetPosition = path[0].worldPosition;
				targetPosition.y = seeker.position.y;
				seeker.LookAt(targetPosition);
				seeker.Translate(new Vector3(0, 0, 1) * Time.deltaTime * 4);
			}
		}
		catch (Exception e)
		{
			
		}
	}

	private List<Node> FindPath(Vector3 startPos, Vector3 targetPos)
	{
		Node startNode = grid.NodeFromWorldPoint(startPos);
		Node targetNode = grid.NodeFromWorldPoint(targetPos);
		
		List<Node> openSet = new List<Node>();
		HashSet<Node> closedSet = new HashSet<Node>();
		openSet.Add(startNode);

		while (openSet.Count > 0)
		{
			Node currentNode = openSet[0];
			for (int i = 0; i < openSet.Count; i++)
			{
				if (openSet[i].fCost < currentNode.fCost ||
				    openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost)
				{
					currentNode = openSet[i];
				}
			}

			openSet.Remove(currentNode);
			closedSet.Add(currentNode);

			if (currentNode == targetNode)
			{
				return RetracePath(startNode, targetNode);
			}

			foreach (Node neighbour in grid.GetNeighbours(currentNode))
			{
				if (neighbour.walkable && !closedSet.Contains(neighbour))
				{
					int MovementCost = currentNode.gCost + GetDistance(currentNode, neighbour);

					if (MovementCost < neighbour.gCost || !openSet.Contains(neighbour))
					{
						neighbour.gCost = MovementCost;
						neighbour.hCost = GetDistance(neighbour, targetNode);
						neighbour.parent = currentNode;

						if (!openSet.Contains(neighbour))
						{
							openSet.Add(neighbour);
						}
					}
				}
			}
		}

		return null;
	}

	private List<Node> RetracePath(Node startNode, Node endNode)
	{
		List<Node> path = new List<Node>();
		Node currentNode = endNode;

		while (currentNode != startNode)
		{
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}

		if (path.Count > 0)
		{
			path.Reverse();
		}

		grid.path = path;

		return path;
	}

	private int GetDistance(Node nodeA, Node nodeB)
	{
		int distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
		int distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

		if (distX > distY)
		{
			return 14 * distY + 10 * (distX - distY);
		}
		
		return 14 * distX + 10 * (distY - distX);
	}
}
