﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver5 : MonoBehaviour {

    #region Attributs 
    public Button RETRY; 
    public Button QUIT;
	
    #endregion 

    void Start () 
    {
		
        RETRY.onClick.AddListener (OnClickRETRY);
        QUIT.onClick.AddListener (OnClickQUIT);
    }
	

    void Update () 
    {
		
    }

    void OnClickRETRY()
    {
        Application.LoadLevel ("Niveau_pacman");
    }

    void OnClickQUIT()
    {
        Application.LoadLevel ("Scene_MenuPrincipal");
    }
}
