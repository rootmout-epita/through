﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver8 : MonoBehaviour {

    #region Attributs 
    public Button RETRY; 
    public Button QUIT;
	
    #endregion 

    void Start () 
    {
		
        RETRY.onClick.AddListener (OnClickRETRY);
        QUIT.onClick.AddListener (OnClickQUIT);
    }
	

    void Update () 
    {
		
    }

    void OnClickRETRY()
    {
        Application.LoadLevel ("Niveau 4");
    }

    void OnClickQUIT()
    {
        Application.LoadLevel ("Scene_MenuPrincipal");
    }
}
