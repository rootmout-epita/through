﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver9 : MonoBehaviour {

    #region Attributs 
    public Button RETRY; 
    public Button QUIT;
	
    #endregion 

    void Start () 
    {
		
        RETRY.onClick.AddListener (OnClickRETRY);
        QUIT.onClick.AddListener (OnClickQUIT);
    }
	

    void Update () 
    {
		
    }

    void OnClickRETRY()
    {
        Application.LoadLevel ("niveau2");
    }

    void OnClickQUIT()
    {
        Application.LoadLevel ("Scene_MenuPrincipal");
    }
}
