﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandeJoueur : MonoBehaviour {

	//public double speed = 600;  // 600 est une vitesse raisonnable pour l'objet ( à débattre en groupe)
	private Transform _t ;
	public float speed;
	
	public float sensivity = 2f;
	CharacterController player;

	public GameObject eyes;

	private float moveFB;
	private float moveLR;

	private float rotX;
	private float rotY;






	/*public float DragSpeed = 2f;
	public bool ReverseDrag = true;

	private Vector3 _DragOrigin;
	private Vector3 _Move;
     */










	// Use this for initialization
	void Start () {
		_t = this.gameObject.GetComponent<Transform> ();

		Vector3 position = new Vector3 (0, 0, 0);
		_t.position = position;	
		
		player = GetComponent<CharacterController>();

	}
	
	// Update is called once per frame
	void Update () {

		/*float DeplacementVertical = Input.GetAxis ("Vertical");        //Input.GetAxis est la fonction qui "détecte" la touche du clavier
		float DeplacementHorizontal = Input.GetAxis ("Horizontal");

		Vector3 mouvement = new Vector3 (DeplacementHorizontal, 0, DeplacementVertical); //Je définis le mouvement du personnage

		rigidbody.AddForce(mouvement * speed * Time.deltaTime); // J'ajoute (j'applique) des forces sur mon objet
																 // Time.deltaTime permet que la vitesse de déplacement de l'objet soit la même et cela même si les nombres de frames per second varient d'un ordinateur à un autre			
		*/
		
		Vector3 direction = Vector3.zero;		

		if (Input.GetKey (KeyCode.LeftArrow)) {
			direction = Vector3.left;
		}

		if (Input.GetKey (KeyCode.RightArrow)) {
			direction = Vector3.right;
		}

		if (Input.GetKey (KeyCode.UpArrow)) {
			direction = Vector3.forward;
		}

		if (Input.GetKey (KeyCode.DownArrow)){
			direction  = Vector3.back;
		}

		_t.position += direction * Time.deltaTime *speed;
		
		
		rotX = Input.GetAxis("Mouse X") * sensivity;
		rotY += Input.GetAxis("Mouse Y") * sensivity;
		rotY = Mathf.Clamp(rotY, -60f, 60f);
		
		
		transform.Rotate(0,rotX, 0);
		eyes.transform.localRotation= Quaternion.Euler(-rotY,0,0);
		
		
		/*if(Input.GetMouseButtonDown(1))
		{
			_DragOrigin = Input.mousePosition;
			//return;
		}

		if(!Input.GetMouseButton(1)) return;

		if(ReverseDrag)
		{
			Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - _DragOrigin);
			_Move = new Vector3(pos.x * DragSpeed, 0, pos.y * DragSpeed);
		}
		else
		{
			Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - _DragOrigin);
			_Move = new Vector3(pos.x * -DragSpeed, 0, pos.y * -DragSpeed);
		}
		transform.Translate(_Move, Space.World);
		*/








		
		



	}
}


