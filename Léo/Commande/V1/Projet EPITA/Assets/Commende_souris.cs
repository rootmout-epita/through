﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class Commende_souris : MonoBehaviour
{

	public float speed = 2f;                //vitesse du joueur
	public float sensivity = 2f;			//sensibilité de la souris
	CharacterController player;

	public GameObject eyes;

	private float moveFB;
	private float moveLR;

	private float rotX;
	private float rotY;
	
	// Use this for initialization
	void Start ()
	{
		player = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
	{

		moveFB = Input.GetAxis("Vertical") * speed;
		moveLR = Input.GetAxis("Horizontal") * speed;

		rotX = Input.GetAxis("Mouse X") * sensivity;
		rotY -= Input.GetAxis("Mouse Y") * sensivity;
		rotY = Mathf.Clamp(rotY, -60f, 60f);
		
		Vector3 movement = new Vector3(moveLR,0,moveFB);
		transform.Rotate(0,rotX, 0);
		eyes.transform.localRotation= Quaternion.Euler(rotY,0,0);

		movement = transform.rotation * movement;
		player.Move(movement * Time.deltaTime);

	}
}
